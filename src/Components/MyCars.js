 import React , { Component} from 'react';
 import Car from "./Cars";
class MyCars extends Component {

    state = {
        voitures:[
            {name:'ford', color:'red', year:2000},
            {name:'mercedes', color:'black', year:2010},
            {name:'peugeot', color:'green', year:2018},
        ]
    }

    noCopy = () => {
        alert('merci de ne pas copier')
    }

    addStyled = (e) => {
        if(e.target.classList.contains('styled')) {
            e.target.classList.remove('styled');

        }
        else{
            e.target.classList.add('styled');
        }
    }
    addYears = () => {
        const updateState = this.state.voitures.map((param)=> {
            return param.year -= 10;
        })
        this.setState({
            updateState
        })
    }
    render() {

       const year  = new Date().getFullYear()
        return (
            <div>
                <h1 onMouseOver={this.addStyled}>{this.props.title}</h1>
                <p onCopy={this.noCopy}>ne pas copierrrrrrrrrrrrrr</p>
                {
                    this.state.voitures.map((voiture, id) => {
                        return(
                            <Car nom={voiture.name} key={id} color={voiture.color} year={ year - voiture.year + ' ans'}/>
                        )
                    })
                }
                <button onClick={this.addYears}>+ 10 ans</button>

            </div>
        )
    }
}
export default MyCars;