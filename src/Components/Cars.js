    import React from 'react';

const Car = ({nom,color, year}) => {
    const colorInfo = color ? (<p>Couleur: {color}</p>) : (<p>Couleur: neant</p>);

    if (nom) {
        return(
            <div style={{backgroundColor: 'pink', width:'400px', padding:'10px', margin:'5px auto'
            }}>
                <p>marque: {nom}</p>
                <p>Age: {year}</p>
                {colorInfo}
            </div>
        )
    }
    else {
        return(
            <div style={{backgroundColor: 'pink', width:'400px', padding:'10px', margin:'5px auto'
            }}>
                <p>pas de data</p>
            </div>
            )

    }
}
export default Car;