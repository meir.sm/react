import React ,{ Component} from 'react';
import './App.css';
import MyCars from "./Components/MyCars";

class App extends Component {

    state = {
        titre: 'Mon catalogue voiture'
    }

    buttonAsBeenClicked = (e) => {
        this.setState({
            titre: 'mon new titre'
        })
    }
    changeViaParam = (titre) => {
        this.setState({
            titre
        })
    }
    changeViaBind = (param) => {
        this.setState({
            titre: param
        })
    }
    changeViaInput = (e) => {
        this.setState({
            titre: e.target.value
        })
    }
    render() {
        return (
            <div className="App">
                <MyCars  title={this.state.titre}/>
                <button onClick={this.buttonAsBeenClicked}> changer le nom en dur</button>
                <button onClick={()=>this.changeViaParam('titre via param')}>via param</button>
                <button onClick={this.changeViaBind.bind(this,'titre via bind')}>via bind</button>
                <input type="text" onChange={this.changeViaInput} value={this.state.titre}/>
            </div>
        );
    }
}

export default App;
